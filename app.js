// Set up the dependencies
// Require is used to relate other files to our current file
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// import routes
// const orderRoutes = require('./routes/orderRoutes');
// const userRoutes = require('./routes/userRoutes');

const orderRoutes = require('./routes/orderRoutes');
const userRoutes = require('./routes/userRoutes');
const productRoute = require('./routes/productRoute')

//Server Setup

const app = express();
const port = 4000;

app.use(cors()); 
// Tells server that cors is being used by your server

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}))


// app.use("/orders", orderRoutes)
// app.use("/users", userRoutes)

app.use("/orders", orderRoutes)
app.use("/users", userRoutes)
app.use("/products",productRoute)


mongoose.connect("mongodb+srv://admin:admin123@cluster0.nzwdx.mongodb.net/bob_order_booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})



mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})