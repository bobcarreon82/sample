//controllers is creating a different feature for our application. Solely responsible for logic. 

const User = require ('../models/user.js');
const Order = require("../models/order");
const auth = require("../auth")
const bcrypt = require ("bcrypt")
const orderController = require('../controllers/orderController')
const productController = require('../controllers/productController')
const Product = require ('../models/product.js');

//A. add a product


module.exports.addProduct =(body) =>{
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		manufacturer: body.manufacturer,
		price: body.price,
		expirationDate: body.expirationDate
	})

		return newProduct.save().then((user,error) => {
		if(error){
			return false; //user not registered
		} else {
			return true; //user registered
		}
	})
}

//B. Retrieve all products


module.exports.getAllActive =() => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}




//C. Retrieve single product
module.exports.getProduct = (assignByParams) => {
	return Product.findById(assignByParams.productID).then(result => {
		return result;
	})
}



//D. Update a specific product
module.exports.updateProduct = (params, body) => {
	let updatedProduct ={
		name: body.name,
		description: body.description,
		manufacturer: body.manufacturer,
		price: body.price,
		expirationDate: body.expirationDate
	}

	return Product.findByIdAndUpdate(params.productID, updatedProduct).then((product, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//E. Archive a product

module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params.productID, updatedProduct).then((product, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}





