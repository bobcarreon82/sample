const User = require ('../models/user.js');
const Order = require("../models/order");
const auth = require("../auth")
const bcrypt = require ("bcrypt")
const orderController = require('../controllers/orderController')
const productController = require('../controllers/productController')
const Product = require ('../models/product.js');

//A. Create or register a user

module.exports.registerUser =(body) =>{
	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		prcNumber: body.prcNumber,
		tin: body.tin,
		deliveryAddress: body.deliveryAddress,
		mobileNo: body.mobileNo,
		email: body.email,
		salesRep: body.salesRep,
		password: bcrypt.hashSync(body.password, 10)
	})

		return newUser.save().then((user,error) => {
		if(error){
			return false; //user not registered
		} else {
			return true; //user registered
		}
	})
}

//B. How to Log In
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null) {
			return false; //user doesn't exist

		} else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())
				}
			}else{
				return false; //passwords didn't match, can't log in

			}		
		}
	})
}


//C. Purchase a product

module.exports.purchase = async(data) => {
	let productSaveStatus = await Product.findById(data.productId).then(product =>{
		product.customers.push({userId: data.userId})
		return product.save().then((product, err) =>{
			if(err){
				return false; 
			} else {
				return true; 
			}
		})
	})

	let userSaveStatus = await User.findById(data.userId).then(user => {
		user.purchase.push({productId: data.productId})
		return user.save().then((user, err)=> {
			if(err){
				return false;
			} else{
				return true; 
			}
		})
	})

	if(productSaveStatus && userSaveStatus){
		return true;
	} else {
		return false;
	}
}


