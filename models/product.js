const mongoose = require("mongoose");//required on top

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true,"name is required"]
	},

	description: {
		type: String,
		required: [true, "description is required"]
	},

	manufacturer: {
		type: String,
		required: [true, "manufacturer is required"]
	},

	price: {
		type: Number,
		required: [true, "price is required"]
	},


	expirationDate: {
		type: String,
		required: [true, "date is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	customers: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			},

			purchasedOn: {
				type: Date,
				default: [true, "date is required"]
			}	
		}

	]
	
})


module.exports = mongoose.model("Product", productSchema);//required on bottom