const mongoose = require ("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},

	prcNumber: {
		type: String,
		required: [false, "Only for doctors"]
	},

	tin: {
		type: String,
		required: [false, "Only for VAT customers"]
	},

	deliveryAddress: [
		{
			address: {
				type: String,
				required: [true, "address is required"]
			},

			cityTown: {
				type: String, 
				required: [true, "city or town is required"]
			}, 

			province: {
				type: String,
				required: [true, "province is required"]
			}

		}

	],

		mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},


	email: {
		type: String,
		required: [true, "Only for those willing to give email"]
	},


	salesRep: {
		type: String,
		required: [true, "Indicate sales rep assigned to customer"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: true
	},

	product: [{
		productId: {
			type: String,
			required: [true, "Product ID required"]
		},

		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}

	]
})

module.exports = mongoose.model("User", userSchema);