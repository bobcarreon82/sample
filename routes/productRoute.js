//set up dependencies
const express = require('express');
const router = express.Router();
const auth = require("../auth");
const orderController = require('../controllers/orderController');
const productController = require('../controllers/productController');
const Product = require ('../models/product.js');


//A. Route to create a new product

router.post("/",  (req, res) =>{
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		productController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	} else {
		res.send({auth: "Not an admin"})
	}
	
})


//B. Retrieve All products


router.get("/all",(req, res) => {
	productController.getAllActive(req.body).then(resultFromGetAllActive => res.send(
		resultFromGetAllActive))
})



//C. Retrieve A Single Product

router.get("/:productID",(req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})


//D. Update a specific product

router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})



//E. Archive a product
router.delete("/:courseId", auth.verify, (req, res)=> {
	productController.archiveProduct(req.params).then(resultFromArchive =>res.send(resultFromArchive))
})





module.exports = router; //bottom of page