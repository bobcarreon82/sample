//set up dependencies
const express = require('express');
const router = express.Router();
const auth = require("../auth");
const userController = require('../controllers/userController') 
const User = require("../models/user.js")
const productController = require('../controllers/productController')
const Product = require ('../models/product.js');
//link to the app.js to link all files together

//A. Route for registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//B. Route for logging in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})



//C. To purchase
router.post("/purchase", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		productID: req.body.productID
	}

	userController.purchase(data).then(resultFromPurchase => res.send(resultFromPurchase))
})




module.exports = router; //bottom of page

